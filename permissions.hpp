#ifndef PERMISSIONS_HPP
#define PERMISSIONS_HPP



#include <string>
#include <map>


enum permissions {
    manage_roles,
    ban_members,
    remove_members,
    change_name,
    change_perms,
    delete_content,
    invite_users,
    send_content
};

static std::map<std::string, permissions> action_requires = {
    {"rolemod",permissions::manage_roles},
    {"ban",permissions::ban_members},
    {"kick",permissions::ban_members},
    {"rename",permissions::change_name},
    {"permsmod",permissions::change_perms},
    {"invite",permissions::invite_users}
};



#endif // PERMISSIONS_HPP
