#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <sstream>

#include "settings.hpp"
#include "exceptions.hpp"
#include "structs.hpp"
#include "data_storage.hpp"
#include "clientproc.hpp"
#include "environment.hpp"
#include "users.hpp"
#include "helpers.hpp"
#include "connection.hpp"
#include "clientproc.hpp"


// Main code
int main() {
    size_t i = 0;
    struct sockaddr_in clientname{}; // Init every single POD, including structs.
    socklen_t addrlen;
    std::bitset<FD_SETSIZE> readfds;

    // Load data
    load_data();

    // Setup connection
    auto theesechannels = channels;
    auto theeseusers = users;
    auto thisconn = connection();

    while (true) {
        // Block until input arrives on one or more active sockets
        readfds = thisconn.sockswitch();

        // Service all the sockets with input pending
        for (i = 0; i < readfds.size(); ++i) {
            if (readfds.test(i)) {
                std::clog << "Processing potential accept on on socket " << i << "..." << std::endl;
                if (i == thisconn.sock) {
                    // Connection request on original socket
                    addrlen = sizeof(clientname);
                    int newsock = accept(thisconn.sock,
                                         reinterpret_cast<struct sockaddr *>(&clientname),
                                         &addrlen);
                    if (newsock < 0) {
                        perror("accept");
                        exit(EXIT_FAILURE);
                    }
                    thisconn.connect(inet_ntoa(clientname.sin_addr), newsock);
                    // Add client
                    struct client_struct thisclient;
                    thisclient.client_ip = inet_ntoa(clientname.sin_addr);
                    thisclient.conn_id = static_cast<unsigned int>(newsock);
                    clients[newsock] = thisclient;
                } else {
                    std::clog << "Processing input on socket " << i << "..." << std::endl;
                    // Data arriving on an already-connected socket
                    try {
                        clients[i].recv_queue.push_back(read_from_client(&clients[i]));
                    } catch (nomessage&) {
                        // Nothing
                    } catch (client_error&) {
                        thisconn.disconnect(clients[i].client_ip, clients[i].conn_id);
                        clients[i].user->deauth(i);
                        clients.erase(i);
                    }
                }
            }
        }

        msg_proc:
        for (auto& [i, thisclient] : clients) {
            if (!thisclient.recv_queue.empty()) {
                try {
                    for (const auto& message : thisclient.recv_queue) {
                        process_message(&thisclient, message);
                    }
                    thisclient.recv_queue.clear();
                } catch (client_error&) {
                    thisconn.disconnect(thisclient.client_ip, thisclient.conn_id);
                    thisclient.user->deauth(i);
                    clients.erase(i);
                    goto msg_proc; // Reset iterator
                }
            }
        }
    }
}
