#ifndef HELPERS_HH
#define HELPERS_HH



#include <string>
#include <string_view>
#include <vector>
#include "structs.hpp"


extern void write_to_client(struct client_struct *thisclient, std::string message);

extern std::string read_from_client(struct client_struct *thisclient);

extern std::string ltrim(std::string_view s);

extern std::string rtrim(std::string_view s);

inline std::string trim(std::string_view s) {
    return ltrim(rtrim(s));
}

extern std::vector<std::string> strsplit(std::string_view s, char delimiter, std::vector<std::string>::size_type times = 0);



#endif // HELPERS_HH
