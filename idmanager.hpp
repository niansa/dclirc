#ifndef IDMANAGER_HPP
#define IDMANAGER_HPP



#include <string>
#include <vector>


extern std::vector<std::string> used_ids;

extern void randinit();

extern std::string randidgen();

extern bool randidfree(const std::string& id);



#endif // IDMANAGER_HPP
