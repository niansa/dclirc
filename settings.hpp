#ifndef SETTINGS_HH
#define SETTINGS_HH



#include <sys/types.h>


const int PORT = 1234;
const int maxmsg = 1024;
const int maxfds = FD_SETSIZE; // Should always be equal or less than FD_SETSIZE
const int maxnamelen = 12;
const char good_chars[] = "abcdefghijklmnopqrstuvwxyzß1234567890<>|,;.:-_!§$%&/()?\\";



#endif // SETTINGS_HH
