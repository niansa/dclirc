#include "structs.hpp"


bool user_struct::broadcast_event(event_struct *thisevent) {
    std::string timestamp_string = std::to_string(thisevent->timestamp);
    if (thisevent->type == events::join) {
        return broadcast("!JOIN " + timestamp_string + ' ' + thisevent->id + ' ' + thisevent->channel->id + ' ' + thisevent->user->id);
    } else if (thisevent->type == events::leave) {
        return broadcast("!LEAVE " + timestamp_string + ' ' + thisevent->id + ' ' + thisevent->channel->id + ' ' + thisevent->user->id);
    } else if (thisevent->type == events::name_change) {
        return broadcast("!RENAME " + timestamp_string + ' ' + thisevent->id + ' ' + thisevent->channel->id + ' ' + thisevent->value);
    } else if (thisevent->type == events::message) {
        return broadcast("!MESSAGE " + timestamp_string + ' ' + thisevent->id + ' ' + thisevent->channel->id + ' ' + thisevent->user->id + ' ' + std::to_string(thisevent->edited) + ' ' + thisevent->value);
    } else if (thisevent->type == events::message_edit) {
        return broadcast("!MESSAGE_EDIT " + timestamp_string + ' ' + thisevent->id + ' ' + thisevent->channel->id + ' ' + thisevent->value);
    } else if (thisevent->type == events::message_delete) {
        return broadcast("!MESSAGE_DELETE " + timestamp_string + ' ' + thisevent->id + ' ' + thisevent->channel->id);
    }
    return false;
}
