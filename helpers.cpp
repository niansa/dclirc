#include <string>
#include <string_view>
#include <vector>
#include <cstdint>
#include <unistd.h>
#include "settings.hpp"
#include "exceptions.hpp"
#include "structs.hpp"


void write_to_client(struct client_struct *thisclient, std::string message) {
    ssize_t nbytes;
    message.push_back('\n');
    nbytes = write(thisclient->conn_id, message.c_str(), message.size());
    // Check result
    if (nbytes != static_cast<ssize_t>(message.size())) {
        throw client_error();
    }
}

std::string read_from_client(struct client_struct *thisclient) {
    // Iniiiiitttititit!!!
    char buffer[maxmsg] = {0x00};
    ssize_t nbytes;

    // Read from client
    nbytes = read(thisclient->conn_id, buffer, maxmsg - 1);

    // Check for error
    if (nbytes < 0) {
        perror("read");
        exit(EXIT_FAILURE);
    } else if (nbytes == 0) {
        throw client_error();
    } else if (nbytes == 1) {
        throw nomessage();
    }

    // Return as string
    std::string res;
    res.assign(buffer, nbytes - 1);
    return res;
}


std::string ltrim(std::string_view s) {
    std::string res;
    bool started = false;
    for (auto it = s.begin(); it != s.end(); it++) {
        if (started) {
            res.push_back(*it);
        } else if (*it != ' ' and *it != '	') {
            started = true;
        }
    }
    return res;
}
std::string rtrim(std::string_view s) {
    std::string res;
    for (auto it = s.end(); it != s.begin(); it--) {
        if (*it != ' ' and *it != '	') {
            for (auto it2 = s.begin(); it2 != it; it2++) {
                res.push_back(*it);
            }
            break;
        }
    }
    return res;
}


std::vector<std::string> strsplit(std::string_view s, char delimiter, std::vector<std::string>::size_type times = 0) {
    std::vector<std::string> to_return;
    decltype(s.size()) start = 0, finish = 0;
    while ((finish = s.find_first_of(delimiter, start)) != std::string_view::npos) {
        to_return.emplace_back(s.substr(start, finish - start));
        start = finish + 1;
        if (to_return.size() == times) { break; }
    }
    to_return.emplace_back(s.substr(start));
    return to_return;
}
