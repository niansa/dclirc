#ifndef STRUCTS_HH
#define STRUCTS_HH



#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <bitset>
#include <algorithm>
#include "goodchars.hpp"
#include "exceptions.hpp"
#include "permissions.hpp"
#include "timestamp.hpp"
#include "idmanager.hpp"
#include "settings.hpp"

struct event_struct;
struct channel_struct;
struct client_struct;
struct user_struct;
#include "environment.hpp"
extern void write_to_client(struct client_struct *thisclient, std::string message);


enum user_statuses { offline, online, afk };

enum events { message, join, leave, name_change, message_edit, message_delete };

struct event_struct {
    std::string id = "0000000000";
    events type;
    int64_t timestamp;
    channel_struct *channel;
    user_struct *user;
    std::string value;
    bool edited = false;
};

struct user_struct {
    std::string id = "0000000000";
    std::string name = "unauthenticated";
    std::string pwd_hash = "invalid";
    user_statuses status = user_statuses::offline;
    std::bitset<maxfds> connections;
    std::map<std::string, channel_struct*> private_channels;
    //std::vector<std::string> private_guilds;
    bool server_op = false;

    void auth(unsigned int conn_id) {
        connections.set(conn_id);
    }
    void deauth(unsigned int conn_id) {
        connections.reset(conn_id);
    }
    bool broadcast(std::string message) {
        bool is_ok = false;
        for (size_t i = 0; i < connections.size(); ++i) {
            if (connections.test(i)) {
                try {
                    write_to_client(&clients[i], message);
                    is_ok = true;
                } catch (client_error&) {/* Handle it!!! */}
            }
        }
        return is_ok;
    }
    bool broadcast_event(event_struct *thisevent); // Defined in structfunctions.cpp
};

struct role_struct {
    std::string id = "0000000000";
    std::string name = "faulty-role";
    std::vector<user_struct*> members;
    std::bitset<sizeof(permissions)> perms;
};

struct channel_struct {
    std::string id = "0000000000";
    std::string name = "faulty-channel";
    std::vector<user_struct*> members;
    std::map<std::string, event_struct> history;
    std::map<user_struct*, std::bitset<sizeof(permissions)>> peruser_perms;
    std::map<std::string, role_struct> roles;
    user_struct* owner;

    bool has_perm(user_struct *user, permissions permission) {
        // Channel owner and server ops are allowed to do everything
        if (user == owner or user->server_op) {
            return true;
        }
        // Users who are not in the channel never have the permission
        if (std::find(members.begin(), members.end(), user) == members.end()) {
            return false;
        }
        // Check if the user exciplitely has the permission
        if (peruser_perms.find(user) != peruser_perms.end()) {
            if (peruser_perms[user].test(permission)) {
                return true;
            }
        }
        // Check if one of the users roles has the permission
        for (const auto& [roleid, thisrole] : roles) {
            if (thisrole.perms.test(permission)) {
                for (const auto& thisrolemember : thisrole.members) {
                    if (*&thisrolemember == user) {
                        return true;
                    }
                }
            }
        }
        // Return no success
        return false;
    }
    inline void broadcast_event_only(event_struct *thisevent) {
        for (auto& member : members) {
            member->broadcast_event(thisevent);
        }
    }
    event_struct* broadcast_event(event_struct *thisevent) {
        broadcast_event_only(thisevent);
        history[thisevent->id] = *thisevent;
        return &history[thisevent->id];
    }
    bool rename(std::string &newname, bool &autofix) {
        // Generate forcibiliy good string
        std::string goodname = make_good_string(newname);
        // Check result
        if (goodname != newname && not autofix) {
            return false;
        } else if (goodname.size() > maxnamelen) {
            return false;
        }
        // Set name
        name = goodname;
        // Broadcast the change
        event_struct thisevent;
        {
            thisevent.id = randidgen();
            thisevent.type = events::name_change;
            thisevent.channel = this;
            thisevent.value = name;
            thisevent.timestamp = get_timestamp();
        }
        broadcast_event(&thisevent);
        return true;
    }
    std::string* send_message(user_struct *user, const std::string& message) {
        // Create event
        event_struct thisevent;
        {
            thisevent.id = randidgen();
            thisevent.type = events::message;
            thisevent.channel = this;
            thisevent.user = user;
            thisevent.value = message;
            thisevent.timestamp = get_timestamp();
        }
        return &(broadcast_event(&thisevent)->id);
    }
    bool delete_message(const std::string& message_id) {
        // Check if message exists is in channels history
        auto oldevent = history.find(message_id);
        if (oldevent == history.end()) {
            return false;
        }
        // Check if event is a message
        if (oldevent->second.type != events::message) {
            return false;
        }
        // Create event
        event_struct thisevent;
        {
            thisevent.type = events::message_delete;
            thisevent.channel = this;
            thisevent.id = message_id;
        }
        // Send event
        broadcast_event_only(&thisevent);
        // Apply changes
        history.erase(oldevent);
        // Return success
        return true;
    }
    bool edit_message(const std::string& message_id, const std::string& new_message) {
        // Check if message exists is in channels history
        auto oldevent = history.find(message_id);
        if (oldevent == history.end()) {
            return false;
        }
        // Check if event is a message
        if (oldevent->second.type != events::message) {
            return false;
        }
        // Create event
        event_struct thisevent;
        {
            thisevent.type = events::message_edit;
            thisevent.channel = this;
            thisevent.id = message_id;
            thisevent.value = new_message;
        }
        // Send event
        broadcast_event_only(&thisevent);
        // Apply changes
        oldevent->second.value = new_message;
        // Mark message as edited
        history[message_id].edited = true;
        // Return success
        return true;
    }
    void add_user(user_struct *user) {
        // Broacast change
        event_struct thisevent;
        {
            thisevent.type = events::join;
            thisevent.channel = this;
            thisevent.user = user;
            thisevent.timestamp = get_timestamp();
        }
        broadcast_event(&thisevent);
        // Apply change
        members.push_back(user);
        user->private_channels[id] = this;
    }
    bool remove_user(user_struct *user) {
        // Check if user is in channel
        auto i = std::find(members.begin(), members.end(), user);
        if (i == members.end()) {
            return false;
        }
        // Broacast change
        event_struct thisevent;
        {
            thisevent.type = events::leave;
            thisevent.channel = this;
            thisevent.user = user;
            thisevent.timestamp = get_timestamp();
        }
        broadcast_event(&thisevent);
        // Remove user from member list
        members.erase(i);
        user->private_channels.erase(user->private_channels.find(id));
        return true;
    }
    bool remove() {
        // Remove all members
        auto members_backup = members;
        for (auto& member : members_backup) {
            remove_user(member);
        }
        // Verify channel has no members anymore
        if (!members.empty()) {
            return false; // What went wrong??
        }
        // Remove channel from channels vector
        channels.erase(channels.find(id));
        // Report success
        return true;
    }
};

struct client_struct {
    unsigned int conn_id = 0;
    std::vector<std::string> recv_queue;
    std::string client_ip = "unknown";
    user_struct *user = new user_struct;
    bool authed = false;
    bool autostringfix = false;

    std::string get_identifier() {
        std::stringstream res;
        res << conn_id << ':' << user->name << '@' << user->id;
        return res.str();
    }
};



#endif // STRUCTS_HH
