#ifndef CONNECTION_HH
#define CONNECTION_HH


#include <iostream>
#include <string>
#include <bitset>
#include <cstddef>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "settings.hpp"




class connection {
    public:
        size_t sock;
        std::bitset<maxfds> fds;

        std::bitset<maxfds> sockswitch() {
            std::bitset<maxfds> res;
            memcpy(&tmpfdset, &fds, sizeof(fd_set));
            if (select(maxfds, &tmpfdset, nullptr, nullptr, nullptr) < 0) {
                perror("select");
                exit(EXIT_FAILURE);
            }
            memcpy(&res, &tmpfdset, sizeof(fd_set));
            return res;
        }

        void connect(std::string ip_addr, size_t thissock) {
            std::clog << "server: " << "connect from host "
                      << ip_addr
                      << " (" << thissock << ')'
                      << std::endl;
            fds.set(thissock);
        }

        void disconnect(std::string ip_addr, size_t thissock) {
            std::clog << "server: " << "disconnect of host "
                      << ip_addr
                      << " (" << thissock << ')'
                      << std::endl;
            fds.reset(thissock);
            close(thissock);
        }

        connection() {
            // Create the socket and set it up to accept connections
            sock = make_socket(PORT);
            if (listen(sock, 1) < 0) {
                perror("listen");
                exit(EXIT_FAILURE);
            }
            // Add to fds
            fds.set(sock);
            // ZERO-out the C-style fd_set
            FD_ZERO(&tmpfdset);
        }


    private:
        fd_set tmpfdset;

        int make_socket(uint16_t port) {
            // Iniiiiiiiiit.....
            int sock;
            struct sockaddr_in name{};
            // Create the socket
            sock = socket(PF_INET, SOCK_STREAM, 0);
            if (sock < 0) {
                perror("socket");
                exit(EXIT_FAILURE);
            }
            // Give the socket a name
            name.sin_family = AF_INET;
            name.sin_port = htons(port);
            name.sin_addr.s_addr = htonl(INADDR_ANY);
            if (bind(sock, reinterpret_cast<struct sockaddr *>(&name), sizeof(name)) < 0) {
                perror("bind");
                exit(EXIT_FAILURE);
            }
            // Return created socket
            return sock;
        }
};



#endif // CONNECTION_HH
