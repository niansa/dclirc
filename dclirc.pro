TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        clientproc.cpp \
        data_storage.cpp \
        environment.cpp \
        goodchars.cpp \
        helpers.cpp \
        idmanager.cpp \
        main.cpp \
        structfunctions.cpp \
        timestamp.cpp \
        users.cpp

HEADERS += \
    clientproc.hpp \
    connection.hpp \
    data_storage.hpp \
    environment.hpp \
    exceptions.hpp \
    goodchars.hpp \
    helpers.hpp \
    idmanager.hpp \
    permissions.hpp \
    settings.hpp \
    structs.hpp \
    timestamp.hpp \
    users.hpp
