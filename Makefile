all:
	g++ -std=c++17 -o dclirc.elf ./*.cpp -Wall -Wextra

clean:
	rm *.elf
