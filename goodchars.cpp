#include <string>
#include "settings.hpp"


bool good_string(const std::string& s) {
    bool is_good;
    for (const auto& character : s) {
        is_good = false;
        for (const auto& testchar : good_chars) {
            if (character == testchar) {
                is_good = true;
                break;
            }
        }
        if (not is_good) {
            return false;
        }
    }
    return true;
}

std::string make_good_string(const std::string& s) {
    std::string res;
    for (const auto& character : s) {
        for (const auto& testchar : good_chars) {
            if (character == testchar) {
                res.push_back(character);
                break;
            }
        }
    }
    return res;
}
