#include <string>
#include <vector>
#include <ctime>
#include <random>
#include <algorithm>


std::vector<std::string> used_ids = {"0000000000"};



void randinit() {
    std::srand(static_cast<uint>(std::time(nullptr)));
}

std::string randidgen() {
    std::string res = "0000000000";
    while (std::find(used_ids.begin(), used_ids.end(), res) != used_ids.end()) {
        res = "";
        for (u_char i = 10; i != 0; i--) {
            res.push_back(std::to_string((rand() % 9) + 1)[0]);
        }
    }
    used_ids.push_back(res);
    return res;
}

bool randidfree(const std::string& id) {
    auto i = std::find(used_ids.begin(), used_ids.end(), id);
    if (i == used_ids.end()) {
        return false;
    } else {
        used_ids.erase(i);
        return true;
    }
}
