#include <iostream>
#include <string>
#include <string_view>
#include <map>
#include <vector>
#include "structs.hpp"
#include "exceptions.hpp"
#include "environment.hpp"
#include "users.hpp"
#include "timestamp.hpp"
#include "permissions.hpp"
#include "helpers.hpp"



static void write_result(struct client_struct *thisclient, std::string command_id, std::string result) {
    write_to_client(thisclient, command_id + ':' + result);
}

static std::string parser(client_struct *thisclient, bool has_arg, const std::string& command, const std::string& arguments) {
    std::string result;
    std::vector<std::string> argssplit;
    auto *private_channels = &thisclient->user->private_channels;

    // Debug
    std::cout << thisclient->get_identifier() << " > " << message << std::endl;

    // Commands that require no arguments
    if (command == "DISCONNECT") { // DISCONNECT
        throw client_error();
    }

    if (command == "ABORT") { // ABORT (DEBUG!!!)
        abort();
    }

    // Commands that require arguments
    else if (!has_arg) {
        // Do nothing (error)
    }

    else if (command == "AUTH") { // AUTH <user id> <password>
        // Split userid and password
        argssplit = strsplit(arguments, ' ', 1);
        // Check details
        if (argssplit.size() != 2) {
            return "BAD_ARGS";
        } else if (!thisclient->authed) {
            // Check userid and password
            std::clog << "Authenticating \"" << argssplit[0] << "\" with password \"" << argssplit[1] << "\"..." << std::endl;
            user_struct *thisuser = check_auth(argssplit[0], argssplit[1]);
            if (thisuser != nullptr) {
                thisclient->user = thisuser;
                thisuser->auth(thisclient->conn_id);
                thisclient->authed = true;
                return "OK";
            } else {
                return "AUTH_FAILED";
            }
        } else {
            return "ALREADY_AUTHED";
        }
        std::clog << "Result: " << result << std::endl;
    }

    else if (command == "GET_USER") { // GET_USER <ID/name>
        user_struct *thisuser = nullptr;
        // Find user
        for (auto& [userid, thatuser] : users) {
            if (userid == arguments or thatuser.name == arguments) {
                thisuser = &thatuser;
                break;
            }
        }
        if (thisuser != nullptr) {
            // Return informations
            return "OK " + thisuser->id + ' ' + std::to_string(thisuser->status) + ' ' + thisuser->name;
        } else {
            // Return error
            return "BAD_USER";
        }
    }

    // Commands that require authentication
    else if (!thisclient->authed) {
        return "AUTH_REQUIRED";
    }

    else if (command == "GET_CHANNEL") { // GET_CHANNEL <"list"/ID>
        result = "OK\n";
        if (arguments == "list") {
            for (const auto& [channelid, thischannel] : thisclient->user->private_channels) {
                result.append(channelid + ' ' + thischannel->name + '\n');
            }
            result.pop_back();
        } else {
            if (private_channels->find(arguments) == private_channels->end()) {
                return "BAD_CHANNEL";
            } else {
                result.append((*private_channels)[arguments]->name);
            }
        }
        return result;
    }

    else if (command == "GET_CHANNEL_LOG") { // GET_CHANNEL_LOG <ID>
        // Check if user is in given channel
        if (private_channels->find(arguments) == private_channels->end()) {
            return "BAD_CHANNEL";
        } else {
            auto *thischannel = (*private_channels)[arguments];
            // Get channel log
            for (auto& [eventid, thisevent] : thischannel->history) {
                thisclient->user->broadcast_event(&thisevent);
            }
            return "OK";
        }
    }

    else if (command == "JOIN_CHANNEL") { // JOIN_CHANNEL <ID>
        // Check if user is in given channel
        if (channels.find(arguments) == channels.end()) {
            return "BAD_CHANNEL";
        } else {
            channels[arguments].add_user(thisclient->user);
            return "OK";
        }
    }

    else if (command == "LEAVE_CHANNEL") { // LEAVE_CHANNEL <ID>
        // Check if user is in given channel
        if (private_channels->find(arguments) == private_channels->end()) {
            return "BAD_CHANNEL";
        } else {
            (*private_channels)[arguments]->remove_user(thisclient->user);
            return "OK";
        }
    }

    else if (command == "MANAGE_CHANNEL") { // MANAGE_CHANNEL <channel ID> <action> [value]
        // Split arguments
        argssplit = strsplit(arguments, ' ', 3);
        // Check details
        if (argssplit.size() < 2) {
            return "BAD_ARGS";
        } else {
            auto *thischannel = (*private_channels)[argssplit[0]];
            // Check if user is in given channel or a server operator
            if (private_channels->find(argssplit[0]) == private_channels->end() or thisclient->user->server_op) {
                return "BAD_CHANNEL";
            }
            // Check permissions
            else if (action_requires.find(argssplit[1]) != action_requires.end() and // Check if action is listed as one that requires permission
                     not thischannel->has_perm(thisclient->user, action_requires[argssplit[1]])) { // Check permissions
                return "PERM_DENIED";
            } else {
                // Actions that require no value
                if (argssplit[1] == "remove") { // Removes entire channel
                    if (thisclient->user == thischannel->owner) {
                        thischannel->remove();
                        return "OK";
                    } else {
                        return "INVALID_VALUE";
                    }
                }
                // Actions that require value
                else if (argssplit.size() != 3) {
                    return "BAD_ARGS";
                }
                else if (argssplit[1] == "kick") { // Removes user from channel
                    // Try to remove the user
                    if (thischannel->remove_user(&users[argssplit[2]])) {
                        return "OK";
                    } else {
                        return "INVALID_VALUE";
                    }
                }
                else if (argssplit[1] == "rename") { // Renames the channel
                    // Try to rename it
                    if (thischannel->rename(argssplit[2], thisclient->autostringfix)) {
                        return "OK";
                    } else {
                        return "INVALID_VALUE";
                    }
                }
                // Bad action
                else {
                    return "BAD_ACTION";
                }
            }
        }
    }

    else if (command == "SEND_MESSAGE") { // SEND_MESSAGE <channel ID> <message>
        // Split channelid and message
        argssplit = strsplit(arguments, ' ', 1);
        // Check amount of args
        if (argssplit.size() != 2) {
            return "BAD_ARGS";
        }
        // Find channel in users channel list
        auto thischannel_tmp = thisclient->user->private_channels.find(argssplit[0]);
        if (thischannel_tmp == thisclient->user->private_channels.end()) {
            return "BAD_CHANNEL";
        }
        auto *thischannel = thischannel_tmp->second;
        // Check if users is privileged
        if (thischannel->has_perm(thisclient->user, permissions::send_content)) {
            // Send message and set result
            return "OK " + *(thischannel->send_message(thisclient->user, argssplit[1]));
        } else {
            // Report error
            return "BAD_PERMS";
        }
    }

    else if (command == "EDIT_MESSAGE") { // EDIT_MESSAGE <channel ID> <message ID> <new message>
        // Split channelid and message
        argssplit = strsplit(arguments, ' ', 2);
        // Check amount of args
        if (argssplit.size() != 3) {
            return "BAD_ARGS";
        }
        // Find channel in users channel list
        auto thischannel_tmp = thisclient->user->private_channels.find(argssplit[0]);
        if (thischannel_tmp == thisclient->user->private_channels.end()) {
            return "BAD_CHANNEL";
        }
        auto *thischannel = thischannel_tmp->second;
        // Check if message exists
        auto thiseventpair = thischannel->history.find(argssplit[1]);
        if (thiseventpair == thischannel->history.end()) {
            return "BAD_MESSAGE";
        }
        // Check if message can be edited by thisclient->user
        else if (thiseventpair->second.user != thisclient->user) {
            return "PERM_DENIED";
        }
        // Edit message
        else {
            // Apply change and set result
            bool res = thischannel->edit_message(argssplit[1], argssplit[2]);
            if (res) {
                return "OK";
            } else {
                return "FAILED";
            }
        }
    }

    else if (command == "DELETE_MESSAGE") { // DELETE_MESSAGE <channel ID> <message ID>
        // Split channelid and message
        argssplit = strsplit(arguments, ' ', 2);
        // Check amount of args
        if (argssplit.size() != 2) {
            return "BAD_ARGS";
        }
        // Find channel in users channel list
        auto thischannel_tmp = thisclient->user->private_channels.find(argssplit[0]);
        if (thischannel_tmp == thisclient->user->private_channels.end()) {
            return "BAD_CHANNEL";
        }
        auto *thischannel = thischannel_tmp->second;
        // Check if message exists
        auto thiseventpair = thischannel->history.find(argssplit[1]);
        if (thiseventpair == thischannel->history.end()) {
            return "BAD_MESSAGE";
        }
        // Check if message can be deleted by thisclient->user
        else if (thiseventpair->second.user != thisclient->user and
                 not thischannel->has_perm(thisclient->user, permissions::delete_content)) {
            return "PERM_DENIED";
        }
        // Edit message
        else {
            // Apply change and set result
            bool res = thischannel->delete_message(argssplit[1]);
            if (res) {
                return "OK";
            } else {
                return "FAILED";
            }
        }
    }

    // Unknown command
    return "BAD_COMMAND";
}

void process_message(struct client_struct *thisclient, std::string_view message) {
    bool has_arg = false;
    std::string result;
    std::vector<std::string> command, tempsplit;

    // Split command
    std::clog << "Splitting command ID..." << std::flush;
    command = strsplit(message, ':', 1);
    std::clog << " Success!" << std::endl;

    if (command.size() < 2) {
        result = "BAD_COMMAND";
    } else {
        std::clog << "Splitting command itself.." << std::flush;
        tempsplit = strsplit(command[1], ' ', 1);
        if (tempsplit.size() > 1) {
            command[1] = tempsplit[0];
            command.push_back(tempsplit[1]);
            has_arg = true;
        }
        std::clog << " Success!" << std::endl;
        std::clog << "Processing command: " << tempsplit[0] << std::endl;
        // Parse
        result = parser(thisclient, has_arg, command[1], command[2]);
    }

    // Write result
    write_result(thisclient, command[0], result);

    // Debug
    std::cout << thisclient->get_identifier() << " < " << command[0] << ':' << result << std::endl;
}
