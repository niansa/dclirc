#ifndef EXCEPTIONS_HH
#define EXCEPTIONS_HH


#include <exception>

class client_error: public std::exception {};
class nomessage: public std::exception {};
class noperms: public std::exception {};


#endif // EXCEPTIONS_HH
