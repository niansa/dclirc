#include <iostream>
#include <string>
#include <string_view>
#include <map>
#include "structs.hpp"
#include "environment.hpp"


user_struct *check_auth(std::string userid, std::string_view password) {
    std::cout << "Finding user \"" << userid << "\"..." << std::flush;
    if (users.find(userid) != users.end()) {
        std::clog << std::endl;
        std::clog << "Loading user struct..." << std::flush;
        user_struct *thisuser = &users[userid];
        std::clog << " Success!" << std::endl;
        std::clog << "Testing read... " << std::flush;
        std::clog << '"' << thisuser->pwd_hash << "\" (works!)" << std::endl;
        // User ID found, check password
        std::clog << "Comparing \"" << thisuser->pwd_hash << "\" with \"" << password << "\"..." << std::flush;
        if (thisuser->pwd_hash == password) {
            std::clog << " Success!" << std::endl;
            return thisuser;
        }
    }
    // Return error
    std::clog << " Failed" << std::endl;
    return nullptr;
}
