#ifndef GOODCHARS_HH
#define GOODCHARS_HH



#include <string>


extern bool good_string(const std::string& s);
extern std::string make_good_string(const std::string& s);



#endif // GOODCHARS_HH
