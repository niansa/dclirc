#include <string>
#include <vector>
#include <map>
#include "structs.hpp"
#include "idmanager.hpp"
#include "environment.hpp"


void load_data() {
    // Dummy channel
    channel_struct thischannel;
    thischannel.id = "8564256235";
    thischannel.name = "test-channel";
    used_ids.push_back(thischannel.id);
    // Dummy users
    user_struct thisuser;
    thisuser.private_channels[thischannel.id] = &(channels[thischannel.id] = thischannel);
    thisuser.id = "1234567890";
    thisuser.name = "Testuser";
    thisuser.pwd_hash = "bla";
    thisuser.server_op = true;
    users[thisuser.id] = thisuser;
    used_ids.push_back(thisuser.id);
    channels[thischannel.id].members.push_back(&users[thisuser.id]);
    channels[thischannel.id].owner = &users[thisuser.id];
    thisuser.id = "3216549870";
    thisuser.name = "Hayo";
    thisuser.pwd_hash = "blub";
    thisuser.server_op = false;
    users[thisuser.id] = thisuser;
    used_ids.push_back(thisuser.id);
    channels[thischannel.id].members.push_back(&users[thisuser.id]);
}
