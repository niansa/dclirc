#include <string>
#include <map>
#include "structs.hpp"

std::map<std::string, user_struct> users;
std::map<std::string, channel_struct> channels;
std::map<int, client_struct> clients;
