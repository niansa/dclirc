#! /usr/bin/env python3
import socket
import sys



def test_auth(username, password):
	# Connect
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect(('localhost', 1234))
	
	# Get user
	sock.send(f":GET_USER {username}\n".encode())
	response = sock.recv(1024).decode("ascii")[:-1].split(' ')
	
	# Check response
	if response[0] == ":BAD_USER":
		return "User does not exist!"
	elif response[0] != ":OK":
		return "Unexpected response: " + str(response)
	userid = response[1]
	
	# Try to authenticate
	sock.send(f":AUTH {userid} {password}\n".encode())
	response = sock.recv(1024).decode("ascii")[:-1].split(' ')
	
	# Check response
	if response[0] == ":AUTH_FAILED":
		return "Wrong password!"
	elif response[0] != ":OK":
		return "Unexpected response: " + str(response)
	
	# Return success
	return "Authentication successfully completed!"


print(test_auth(sys.argv[1], sys.argv[2]))
