#ifndef USERS_HH
#define USERS_HH



#include <string>
#include <string_view>
#include <map>
#include "structs.hpp"


extern user_struct *check_auth(std::string userid, std::string_view password);



#endif // USERS_HH
